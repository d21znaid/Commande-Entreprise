import argparse
import glob
import os
import pdf2image
import simplejson
import tqdm
import multiprocessing as mp
import cv2
import numpy as np



from network import FIELDS, FIELD_TYPES
from network import util


def process_file(filename, out_dir, phase, ocr_engine):
    try:
        # page = pdf2image.convert_from_path(filename)[0]
        # page.save(os.path.join(out_dir, phase, os.path.basename(filename)[:-3] + 'jpg'))
        im = cv2.imread(filename)
        cv2.imwrite(os.path.join(out_dir, phase, os.path.basename(filename)[:-3] + 'jpg'),im)
        if phase != 'test':
            h, w, c = im.shape
            height = h
            width = w

            ngrams = util.create_ngrams(im, height=height, width=width, ocr_engine=ocr_engine)
            for ngram in ngrams:
                if "amount" in ngram["parses"]:
                    ngram["parses"]["amount"] = util.normalize(ngram["parses"]["amount"], key="amount")
                if "date" in ngram["parses"]:
                    ngram["parses"]["date"] = util.normalize(ngram["parses"]["date"], key="date")

            with open(filename[:-3] + 'txt', 'r') as fp:
                labels = simplejson.loads(fp.read())

            fields = {}
            for field in FIELDS:
                if field in labels:
                    if FIELDS[field] == FIELD_TYPES["amount"]:
                        fields[field] = util.normalize(labels[field], key="amount")
                    elif FIELDS[field] == FIELD_TYPES["date"]:
                        fields[field] = util.normalize(labels[field], key="date")
                    else:
                        fields[field] = labels[field]
                else:
                    fields[field] = ''

            data = {
                "fields": fields,
                "nGrams": ngrams,
                "height": height,
                "width": width,
                "filename": os.path.abspath(
                    os.path.join(out_dir, phase, os.path.basename(filename)[:-3] + 'jpg'))
            }

            with open(os.path.join(out_dir, phase, os.path.basename(filename)[:-3] + 'json'), 'w') as fp:
                fp.write(simplejson.dumps(data, indent=2))
            return True

    except Exception as exp:
        print("Skipping {} : {}".format(filename, exp))
        return False


def main():
    ap = argparse.ArgumentParser()

    ap.add_argument("--data_dir", type=str, default='dataset/',
                    help="path to directory containing prepared data")
    ap.add_argument("--out_dir", type=str, default='processed_data/',
                    help="path to save prepared data")
    ap.add_argument("--val_size", type=float, default=0.2,
                    help="validation split ration")
    ap.add_argument("--cores", type=int, help='Number of virtual cores to parallelize over',
                    default=max(1, (mp.cpu_count() - 2) // 2))  # To prevent IPC issues
    ap.add_argument("--ocr_engine", type=str, default='pytesseract',
                    help='OCR used to extract text', choices=['pytesseract', 'aws_textract'])

    args = ap.parse_args()

    os.makedirs(os.path.join(args.out_dir, 'train'), exist_ok=True)
    os.makedirs(os.path.join(args.out_dir, 'test'), exist_ok=True)
    os.makedirs(os.path.join(args.out_dir, 'val'), exist_ok=True)

    filenames = [os.path.abspath(f) for f in glob.glob(args.data_dir + "**/*.jpg", recursive=True)]

    idx = int(len(filenames) * args.val_size)
    train_files = filenames[idx:]
    val_and_train_files = filenames[:idx]
    val_files,test_files = np.array_split(val_and_train_files,2)

    print("Total: {}".format(len(filenames)))
    print("Training: {}".format(len(train_files)))
    print("Validation: {}".format(len(val_files)))
    print("Testing: {}".format(len(test_files)))

    for phase, filenames in [('test', test_files),('train', train_files), ('val', val_files)]:
        print("Preparing {} data...".format(phase))
        

        with tqdm.tqdm(total=len(filenames)) as pbar:
            pool = mp.Pool(args.cores)
            for filename in filenames:
                 
                pool.apply_async(process_file, args=(filename, args.out_dir, phase, args.ocr_engine),
                                callback=lambda _: pbar.update())

            pool.close()
            pool.join()


if __name__ == '__main__':
    main()
