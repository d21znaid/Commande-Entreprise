
FIELD_TYPES = {
    "general": 0,
    "optional": 1,
    "amount": 2,
    "date": 3
}

FIELDS = dict()

FIELDS["address"] = FIELD_TYPES["general"]
FIELDS["company"] = FIELD_TYPES["general"]
FIELDS["date"] = FIELD_TYPES["general"]
#FIELDS["net_amount"] = FIELD_TYPES["amount"]
#FIELDS["tax_amount"] = FIELD_TYPES["amount"]
FIELDS["amount"] = FIELD_TYPES["general"]

'''
FIELD_TYPES = {
    "general": 0,
    "optional": 1,
    "amount": 2,
    "date": 3
}

FIELDS = dict()

FIELDS["invoice_number"] = FIELD_TYPES["general"]
FIELDS["vendor_name"] = FIELD_TYPES["general"]

FIELDS["invoice_date"] = FIELD_TYPES["date"]

FIELDS["net_amount"] = FIELD_TYPES["amount"]
FIELDS["tax_amount"] = FIELD_TYPES["amount"]
FIELDS["total_amount"] = FIELD_TYPES["amount"]
'''
