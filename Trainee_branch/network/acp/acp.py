
import os

import tensorflow as tf
from tensorflow.keras.models import load_model 
from ..model import Model
from .data import InvoiceData
from .model import AttendCopyParseModel
from .. import FIELD_TYPES, FIELDS
from network.parsing.parsers import DateParser, AmountParser, NoOpParser, OptionalParser
 

class AttendCopyParse(Model):
   

    def __init__(self, field, trained_loss,
        trained_accuracy,restore=False):
        
        # Define our metrics

        
        self.field = field
        self.trained_loss = trained_loss
        self.trained_accuracy = trained_accuracy

        self.restore_all_path = './models/{}/best'.format(self.field) if restore else None
        os.makedirs("./models/", exist_ok=True)

        if FIELDS[field] == FIELD_TYPES["optional"]:
            noop_parser = NoOpParser()
            parser = OptionalParser(noop_parser, 128)
        elif FIELDS[field] == FIELD_TYPES["amount"]:
            parser = AmountParser()
        elif FIELDS[field] == FIELD_TYPES["date"]:
            parser = DateParser()
        else:
            parser = NoOpParser()

        restore = parser.restore()
        if restore is not None:
            print("Restoring %s parser %s..." % (self.field, restore))
            tf.train.Checkpoint(model=parser).read(restore).expect_partial()

        self.model = AttendCopyParseModel(parser=parser)

        self.loss_object = tf.keras.losses.SparseCategoricalCrossentropy(
            from_logits=True,
            reduction=tf.keras.losses.Reduction.NONE)

        self.optimizer = tf.keras.optimizers.Nadam(learning_rate=3e-4)

        self.model.compile(self.optimizer)

        self.checkpoint = tf.train.Checkpoint(optimizer=self.optimizer, model=self.model)

        if self.restore_all_path:
            if not os.path.exists('./models/{}'.format(self.field)):
                raise Exception("No trained model available for the field '{}'".format(self.field))
            print("Restoring all " + self.restore_all_path + "...")
            self.checkpoint.read(self.restore_all_path).expect_partial()

    def loss_func(self, y_true, y_pred):
        mask = tf.cast(tf.logical_not(tf.equal(y_true, InvoiceData.pad_idx)), dtype=tf.float32)  # (bs, seq)
        label_cross_entropy = tf.reduce_sum(
            self.loss_object(y_true, y_pred) * mask, axis=1) / tf.reduce_sum(mask, axis=1)
        field_loss = tf.reduce_mean(label_cross_entropy)
        loss = field_loss + sum(self.model.losses)
        return loss
    
    def trained_loss(self):
        return self.trained_loss
    
    def trained_accuracy(self):
        return self.trained_accuracy
    
    def field(self):
            return self.field


    @tf.function
    def train_step(self, inputs):
        inputs, targets = inputs[:-1], inputs[-1]
        with tf.GradientTape() as tape:
            predictions = self.model(inputs, training=True)
            loss = self.loss_func(targets, predictions)
        gradients = tape.gradient(loss, self.model.trainable_variables)
        self.optimizer.apply_gradients(zip(gradients, self.model.trainable_variables))
        self.trained_loss(loss)
        self.trained_accuracy(targets,predictions)
        return loss

    @tf.function
    def val_step(self, inputs):
        inputs, targets = inputs[:-1], inputs[-1]
        predictions = self.model(inputs, training=False)
        loss = self.loss_func(targets, predictions)
        return loss

    def predict(self, paths):
        data = InvoiceData(field=self.field)
        shapes, types = data.shapes()[:-1], data.types()[:-1]

        def _transform(i, v, s, *args):
            return (tf.SparseTensor(i, v, s),) + args

        dataset = tf.data.Dataset.from_generator(
            data.generate_test_data(paths),
            types,
            shapes
        ).map(_transform) \
            .batch(batch_size=1, drop_remainder=False)

        predictions = []
        for sample in dataset:
            try:
                logits = self.model(sample, training=False)
                chars = tf.argmax(logits, axis=2, output_type=tf.int32).numpy()
                predictions.extend(data.array_to_str(chars))
            except tf.errors.OutOfRangeError:
                break

        return predictions

    def save(self, name):
        self.checkpoint.write(file_prefix="./models/%s/%s" % (self.field, name))
        print("saving ....")

        
        

    def load(self, name):
        self.checkpoint.read(name)
