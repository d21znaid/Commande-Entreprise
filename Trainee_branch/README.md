<a href="https://afd.tech">
    <img src="https://github.com/Rems95/mate-storage/raw/main/mate_logo.png" alt="Aimeos logo" title="Mate" align="right" height="60" />
</a>

# MATE-INVOICE-READER


[MATE](https://afd.tech)  est l'application d' AFD.TECH pour les collaborateurs, par les collaborateurs.
 
MATE-INVOICE-READER permettra d'automatisé le processus d'extraction des champs clés des factures pour les notes des collaborateurs. 


## Table des matières

- [Architecture](#architecture)
- [Installation](#installation)
    - [Prérequis ](#prérequis)
    - [Cloner le depot ](#git_clone)
    - [Dépendance](#dépendance)
- [Traitement des données](#traitementdesdonnées)
    - [Datasets](#datasets)
    - [Traitement et séparation des données](#Traitement_et_séparation_des_données)
- [Entraînement du modèle](#Entraînement_du_modèle)
- [Visualisation](#visualisation)
- [Prédiction](#prédiction)
- [Lien](#lien)
- [TODO](#todo)

## Architecture

    .
    ├── README.md
    ├── dataset
    │   ├── 604.json
    │   ├── 605.jpg
    │   ├── 605.json
    │   ├── 606.jpg
    │   ├── 606.json
    │   ├**********
    │   ├**********
    │   ├**
    │   ├**
    ├── logs
    │   └── gradient_tape
    │       ├── 20221125-092713
    │       │   └── train_amount
    │       │       └── events.out.tfevents.1669364833.macbook-pro.v2
    │       ├── 20221125-112930
    │       │   └── train_address
    │       │       └── events.out.tfevents.1669372170.macbook-pro.v2
    │       ├── 20221125-115649
    │       │   └── train_date
    │       │       └── events.out.tfevents.1669373809.macbook-pro.v2
    │       ├── 20221125-123854
    │       │   └── train_company
    │       │       └── events.out.tfevents.1669376334.macbook-pro.v2
    │       └── 20221129-140927
    │           └── train_amount
    │               └── events.out.tfevents.1669727367.macbook-pro.v2
    ├── mate-venv
    ├── models
    │   ├── address
    │   │   ├── best.data-00000-of-00001
    │   │   └── best.index
    │   ├── amount
    │   │   ├── best.data-00000-of-00001
    │   │   └── best.index
    │   ├── company
    │   │   ├── best.data-00000-of-00001
    │   │   └── best.index
    │   └── date
    │       ├── best.data-00000-of-00001
    │       └── best.index
    ├── network
    │   ├── __init__.py
    │   │   ├── __init__.cpython-38.pyc
    │   │   ├── data.cpython-38.pyc
    │   │   ├── model.cpython-38.pyc
    │   │   ├── trainer.cpython-38.pyc
    │   │   └── util.cpython-38.pyc
    │   ├── acp
    │   │   ├── __init__.py
    │   │   │   ├── __init__.cpython-38.pyc
    │   │   │   ├── acp.cpython-38.pyc
    │   │   │   ├── data.cpython-38.pyc
    │   │   │   └── model.cpython-38.pyc
    │   │   ├── acp.py
    │   │   ├── data.py
    │   │   └── model.py
    │   ├── data.py
    │   ├── model.py
    │   ├── parsing
    │   │   │   └── parsers.cpython-38.pyc
    │   │   ├── data.py
    │   │   ├── parser.py
    │   │   └── parsers.py
    │   ├── trainer.py
    │   └── util.py
    ├── predict.py
    ├── predictions
    │   ├── 610.json
    │   ├── 611.json
    │   ├── 612.json
    │   ├── 613.json
    │   ├── 614.json
    │   ├**********
    │   ├**********
    │   └── invoice_test.json
    ├── prepare_data.py
    ├── processed_data
    │   ├── test
    │   │   ├── 610.jpg
    │   │   ├── 611.jpg
    │   │   ├── 612.jpg
    │   │   ├── 613.jpg
    │   │   ├── 614.jpg
    │   │   ├── invoice_test.jpg
    │   │   └── ticket5.png
    │   ├── train
    │   │   ├── 594.jpg
    │   │   ├── 594.json
    │   │   ├── 595.jpg
    │   │   ├── 595.json
    │   │   ├**********
    │   │   ├**********
    │   │   ├**********
    │   │   ├**********
    │   └── val
    │       ├── 605.json
    │       ├── 606.jpg
    │       ├**********
    │       └── 607.json
    ├── requirements.txt
    └── train.py

## Installation

### Prérequis
Python 3.7 ou +;
Tesseract OCR (Voir la documentation pour l'installation selon votre OS  https://tesseract-ocr.github.io/tessdoc/Installation.html)

### Cloner le depot 

    git clone https://github.com/dfrancesAfd/Mateo.git
    cd Mateo
    git checkout feature/train
    

### Dépendance

    pip install virtualenv (si vous ne l'avez pas d'installer )
    
    virtualenv mate-venv (Pour créer un environnement virtuel Ex:mate-venv)
    
    source mate-venv/bin/activate (Activer l'environnement virtuel)
    
    pip install -r requirements.txt (Installer toutes les dépendances dans votre environnement virtuel)


## Traitement des données

### Dataset

Comme premier jeu de données, nous avons utilisé les données du SROIE 2019 disponibles ici [SROIE 2019](https://drive.google.com/file/d/1-EJL-1MVnOBFv7QHWnexAO1jVTTgSjNO/view?usp=share_link) il s'agit d'un ensemble de photos de factures en jpg et des mots clé en format json.

### Traitement et séparation des données

 - Télécharger et dézipper le fichier télécharger
 - Mettre le contenu du dossier "0325updated.task2train(626p)" dézippé dans le dossier dans le dossier "train_data"
 - Séparer les données en jeu de "train","test","val" avec 
 `python prepare_data.py` par défaut les données traitée seront stockées dans le dossier "processed_data" 
 

### Entraînement du modèle

Une fois les données traitées on peut passer la l'entraînement de nos modèles.

    python train.py --field entrez-le-champs-ici --batch_size entrez-le-batch_size-ici
    
    batch_size=8 (Recommandé)
    
    # Par exemple, pour les champs'amount','address',etc .. 
    python train.py --field address  --batch_size 8  
    python train.py --field date  --batch_size 8 
    python train.py --field company  --batch_size 8
    python train.py --field amount  --batch_size 8
    
    
  Vous pouvez définir le nombre d'epoch avec l'argument --steps
  par défaut le nombre d'epochs est de 5000.


    # Par exemple, pour 40000 epochs 
    python train.py --field address  --batch_size 8  --steps 40000




### Visualisation avec Tensorboard

Tensorboard est un serveur web permettant de visualiser l'entraînement d'un modèle. 
Vous pouvez commencez à visualiser vos courbes après le démarrage de la phase d'entraînement.

Pour déménager le serveur Tensorboard lancer la commande suivantes: 
```
tensorboard --logdir logs/gradient_tape
```

![enter image description here](https://github.com/Rems95/mate-storage/raw/main/Capture%20d%E2%80%99%C3%A9cran%202022-11-29%20%C3%A0%2015.19.57.png)
Aller sur http://localhost:*****/  l'adresse ip et le port sont précisé dans le terminal.

![enter image description here](https://github.com/Rems95/mate-storage/raw/main/Capture%20d%E2%80%99%C3%A9cran%202022-11-29%20%C3%A0%2015.20.34.png)
 

### Prédiction
Après avoir entraîné un modèle vous pouvez désormais faire des prédictions sur image de facture

-Prédire une image

   
    python predict.py --field entrez-le-champs-ici --invoice path-to-invoice-file  
    
    # Par exemple, pour extraire le champs amount d'une image de facture /Users/joe/Desktop/invoice.jpg
    
    python predict.py --field total_amount --invoice /Users/joe/Desktop/invoice.jpg

  -Prédire toutes les images d'un dossier
  

    # Pour directement prédire les données de test 
    python predict.py --field entrez-le-champs-ici 
    
    # For example, for field 'amount' pour les facture stocker dans un dossier  /Users/joe/Desktop/invoice.jpg/predict_data/ 
    
    python predict.py --field amount --data_dir /Users/joe/Desktop/invoice.jpg/predict_data/


Les predictions seront affichés dans la console et stockées dans le dossiers predictions dans des fichiers json portant le même nom que le fichier qui a été prédit. 



 

## Lien

* [Tensorflow](http://tensorflow.org)
* [Utiliser Tensorboard sans la méthode "fit"](https://www.tensorflow.org/tensorboard/get_started#using_tensorboard_with_other_methods)
* [Fortement inspiré de ce repo](https://github.com/naiveHobo/InvoiceNet)

## TODO

 - [ ] Calculer la test_loss et test_accuracy
 - [ ] Trouver encore plus de datasets
 - [ ] Data augmentation
 - [ ] Serving (Tensorflow server ou Flask ?)

