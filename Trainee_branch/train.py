
import os
import argparse

from network import FIELDS
from network import trainer
from network.acp.acp import AttendCopyParse
from network.acp.data import InvoiceData
import tensorflow as tf

def main():
    ap = argparse.ArgumentParser()

    ap.add_argument("--field", type=str,  choices=FIELDS.keys(),
                    help="field to train parser for")
    ap.add_argument("--batch_size", type=int, default=8,
                    help="batch size for training")
    ap.add_argument("--restore", action="store_true",
                    help="restore from checkpoint")
    ap.add_argument("--data_dir", type=str, default='dataset/',
                    help="path to directory containing prepared data")
    ap.add_argument("--steps", type=int, default=5000,
                    help="maximum number of training steps")
    ap.add_argument("--early_stop_steps", type=int, default=0,
                    help="stop training if validation doesn't improve "
                         "for a given number of steps, disabled when 0 (default)")

    args = ap.parse_args()

    dataset = InvoiceData.create_dataset(field=args.field,
                                            data_dir=os.path.join(args.data_dir, 'train/'),
                                            batch_size=args.batch_size)
    val_data = InvoiceData.create_dataset(field=args.field,
                                          data_dir=os.path.join(args.data_dir, 'val/'),
                                          batch_size=args.batch_size)
    
    trained_loss = tf.keras.metrics.Mean('train_loss_'+args.field, dtype=tf.float32)
    trained_accuracy = tf.keras.metrics.SparseCategoricalAccuracy('train_accuracy_'+args.field)

    print("Training...")
    trainer.train(
        model=AttendCopyParse(field=args.field,trained_loss=trained_loss,
        trained_accuracy=trained_accuracy, restore=args.restore),
        dataset=dataset,
        val_data=val_data,
        total_steps=args.steps,
        early_stop_steps=args.early_stop_steps,
    )


if __name__ == '__main__':
    main()
